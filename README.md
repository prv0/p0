# p0 (anonweb)

This repo is a demonstration on how to use gitlab-ci to generate a static web 
and publish it anonymously in the pages of one or more "disposable" repos.

Everything is accomplished using [Tor](https://www.torproject.org/) and 
preserving the anonimity of the person in charge of the web page.

For more info, see the presentation itself: https://bit.do/anonweb

## Note on reusing these slides

Feel free to reuse these slides. In the spirit of anonimity, however, I wouldn't mind
if you do **not** attribute the source ;-)

## Note on this project visibility

This project is publicly visible for demonstration purposes, but
in a real case, the visibility of this repo would be set to `Private`, 
in order to hide the main repo.
The public would only be able to see the "burner" repo, which is disposable

## Note on the html generator

This demo uses [reveal.js](https://github.com/hakimel/reveal.js/) 
to generate the static html.

Note that any other generator can be used see here for 
[ready-to-clone examples](https://gitlab.com/pages).

In this case, the [reveal.js](https://github.com/hakimel/reveal.js/) repo
was forked and only 3 files were modified:
 - `index.html` was modified with the anonweb slides
 - The `README.md` was replaced by this text (the original one was moved to [README-reveal.md](README-reveal.md) )
 - `.gitlab-ci.yml` was added to do the automatic generation and deployment

